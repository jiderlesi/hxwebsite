import * as fs from "node:fs/promises";

fs.readdir(".output/public/_nuxt").then((files) => {
	fs.open(".output/public/javascript.html", "w").then(async (fp) => {
		fp.write(`<table id="jslicense-labels1">\n`);
		for (let file of files) {
			if (/mjs/.test(file)) {
				await fp.write(
					`\t<tr>\n\t\t<td><a href="/_nuxt/${file}">${file}</a></td>\n\t\t<td><a href="http://www.gnu.org/licenses/gpl-3.0.html">GNU-GPL-3.0-or-later</a></td>\n\t\t<td><a href="/_nuxt/${file}">${file}</a></td>\n\t</tr>\n`
				);
			}
		}
		fp.write(`</table>\n`);
	});
});
