import { defineNuxtConfig } from "nuxt";

// Author: Haoxiang Zhao
// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
	css: [
		"vuetify/lib/styles/main.sass",
		"@mdi/font/css/materialdesignicons.css",
	],
	build: {
		transpile: ["vuetify"],
	},
	vite: {
		define: {
			"process.env.DEBUG": false,
		},
		server: {
			watch: {
				usePolling: true,
			},
		},
		build: {
			minify: false,
		},
	},
});
